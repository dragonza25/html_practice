$(document).bind('ready', function() {
    var wrapper = $('#right');
    var transitionEvent = whichTransitionEvent();
    var currPage = "firstpage";

    //clear hash on refresh page
    if(window.location.hash !== "")
        window.location.href = window.location.href.split('#')[0];
    
    //get the hash before hash change
    $('.main-menu-item').delegate('a','click',function() {
        if(currPage!="firstpage")
            currPage = (window.location.hash).slice(1);
    });

    //show the screen according to the hash
    $(window).on('hashchange', function() {
        //get the hash after hash change
        var triggeredHash = (window.location.hash).slice(1);
        
        console.log(currPage,triggeredHash); //old screen and new screen

        //push the old screen out of the window
        $('.'+currPage).css({
            "transform": "translate(900px,0)"
        });
        
       // openFrame();

        //pull the new screen in the window after the old get pushed out
        $('.'+currPage).one(transitionEvent, function(event) {
            openFrame();
        });
 
         // hide all frame

        //direct to a specific frame
        function openFrame(){
            switch (triggeredHash) {
                case "firstpage":
                    $('.firstpage').css({
                           "transform": "translate(0,0)"
                    });
                    break;
                case "main":
                    $('.main').css({
                           "transform": "translate(0,0)"
                    });
                    break;
                case "ourdog":
                    $('.ourdog').css({
                           "transform": "translate(0,0)"
                    });
                    break;
                case "history":
                    $('.history').css({
                           "transform": "translate(0,0)"
                    });
                    break;
                case "forsale":
                    $('.forsale').css({
                           "transform": "translate(0,0)"
                    });
                    break;
                case "links":
                    $('.links').css({
                           "transform": "translate(0,0)"
                    });
                    break;
                case "contacts":
                    $('.contacts').css({
                           "transform": "translate(0,0)"
                    });
                    break;
            }
            
            currHash = triggeredHash;
        }
        //add status for #right
        if(triggeredHash == "firstpage")
             wrapper.removeClass('open');
        else
            wrapper.addClass('open');
        

    });
    
    
    // Function from David Walsh: http://davidwalsh.name/css-animation-callback
    function whichTransitionEvent(){
      var t,
          el = document.createElement("fakeelement");
    
      var transitions = {
        "transition"      : "transitionend",
        "OTransition"     : "oTransitionEnd",
        "MozTransition"   : "transitionend",
        "WebkitTransition": "webkitTransitionEnd"
      };
    
      for (t in transitions){
        if (el.style[t] !== undefined){
          return transitions[t];
        }
      }
    }

});